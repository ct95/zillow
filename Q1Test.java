import org.junit.*;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class Q1Test {
    Q1 q1;
    String test1, test2, test3, test4, test5, test6, test7, test8, test9, test10, test11;

    @Before
    public void setUp() throws Exception {
        q1 = new Q1();
        test1 = "12345";
        test2 = "023456";
        test3 = "-23421";
        test4 = "123.45";
        test5 = "-123.45";
        test6 = "-233-1";
        test7 = "98a098";
        test8 = "/09/ts";
        test9 = "00000098765";
        test10 = "00200400600800";
        test11 = " 0 ";
    }

    @Test
    public void testStringToLong() throws Exception {
        assertEquals(q1.stringToLong(test1), 12345);
        assertEquals(q1.stringToLong(test2), 23456);
        assertEquals(q1.stringToLong(test3), -23421);
        assertEquals(q1.stringToLong(test9), 98765);
        assertEquals(q1.stringToLong(test10), Long.parseLong("200400600800"));
        assertEquals(q1.stringToLong(test11), 0);
    }

    @Test
    public void testValidNumber() throws Exception {
        assertTrue(q1.validNumber(test1));
        assertTrue(q1.validNumber(test2));
        assertTrue(q1.validNumber(test3));
        assertFalse(q1.validNumber(test4));
        assertFalse(q1.validNumber(test5));
        assertFalse(q1.validNumber(test6));
        assertFalse(q1.validNumber(test7));
        assertFalse(q1.validNumber(test8));
        assertTrue(q1.validNumber(test9));
        assertTrue(q1.validNumber(test10));
        assertTrue(q1.validNumber(test11.trim()));
    }
}
