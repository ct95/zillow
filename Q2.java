
public class Q2 {

    public void insert(int value, TNode node) {
        if(node == null) node = new TNode(value);
        if(value == node.value) {
            if(node.cntr != null) insert(value, node.cntr);
            else node.cntr = new TNode(value);
        }
        else if(value > node.value) {
            if(node.right != null) insert(value, node.right);
            else node.right = new TNode(value);
        }
        else{
            if(node.left != null) insert(value, node.left);
            else node.left = new TNode(value);
        }
    }

    public void delete(int value, TNode node){
        if(node == null) return;
        else if(value == node.value) {
            if(node.cntr == null) node = null;
            else delete(value, node.cntr);
        }
        else if(value > node.value){
            if(node.right != null && value == node.right.value) {
                if(node.right.cntr == null) node.right = null;
            }else delete(value, node.right);
        }
        else if(value < node.value){
            if(node.left != null && value == node.left.value) {
                if(node.left.cntr == null) node.left = null;
            }else delete(value, node.left);
        }
    }

}
