import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Q2Test {
    Q2 q2;
    TNode root, left1, cntr1, right1, left2, cntr2, right2, left3, cntr3, right3;

    @Before
    public void setUp() throws Exception {
        q2 = new Q2();
        left2 = new TNode(1);
        cntr2 = new TNode(2);
        right2 = new TNode(3);
        left1 = new TNode(2, left2, cntr2, right2);
        cntr1 = new TNode(5);
        left3 = new TNode(6);
        cntr3 = new TNode(7);
        right3 = new TNode(8);
        right1 = new TNode(7, left3, cntr3, right3);
        root = new TNode(5, left1, cntr1, right1);
    }
    @Test
    public void testInsert() throws Exception {
        q2.insert(5, root);
        assertTrue(root.cntr.cntr != null);
        assertTrue(root.cntr.cntr.value == 5);
        q2.insert(1, root);
        assertTrue(root.left.left.left == null);
        assertTrue(root.left.left.cntr != null);
        assertTrue(root.left.left.right == null);
        assertTrue(root.left.left.value == 1);
        q2.insert(10, root);
        assertTrue(root.right.right.right != null);
        assertTrue(root.right.right.cntr == null);
        assertTrue(root.right.right.left == null);
        assertTrue(root.right.right.right.value == 10);
    }

    @Test
    public void testDelete() throws Exception {
        q2.delete(0, root);
        assertTrue(root.left.left.left == null);
        q2.delete(1, root);
        assertTrue(root.left.left == null);
        q2.delete(8, root);
        assertTrue(root.right.right == null);
        q2.delete(6, root);
        assertTrue(root.right.left == null);
        TNode.printout(root);

    }
}
