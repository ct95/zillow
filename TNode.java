
public class TNode {
    int value;
    TNode left, cntr, right;

    public TNode(int value) {
        this.value = value;
        this.left = null;
        this.cntr = null;
        this.right = null;
    }

    public TNode(int value, TNode left, TNode cntr, TNode right) {
        this.value = value;
        this.left = left;
        this.cntr = cntr;
        this.right = right;
    }

    public static void printout(TNode node){
        if(node != null) {
            System.out.println(node.value);
            printout(node.left);
            printout(node.cntr);
            printout(node.right);
        }
    }
}
