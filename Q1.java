
public class Q1 {

    public long stringToLong(String s) {
        s = s.trim();     // take out redundant spaces
        long rtn = 0;
        if(validNumber(s)) {
            if(s.charAt(0) == '-') {
                return -1 * stringToLong(s.substring(1));
            }else{
                for(int i = 0; i<s.length() ; i++) {
                    rtn += Math.pow(10, s.length() - 1- i) * (s.charAt(i) - 48);
                }
            }
        }
        return rtn;
    }

    public boolean validNumber(String s) {
        for(int i = 0; i<s.length() ; i++) {
            if(i == 0 && s.charAt(i) == 45) continue;  // wild card for negative sign
            else if(s.charAt(i) < 48 || s.charAt(i) > 57) {   // ascii code '0' = 48, '9' = 57
                return false;
            }
        }
        return true;
    }

}
